<?php require('config.php'); ?><html>
	<head>
		<base href="//<?php echo $base_href; ?>/" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<meta name="keywords" content=""/>
		<meta name="description" content="<?php echo $base_href; ?> - fotogaleria" />
		<meta name="Robots" content="index, follow" />
		<meta name="Classification" content="global,all" />
		<meta name="Revisit-after" content="2 days" />
		<title>galeria</title>
		<link rel="shortcut icon" href="favicon.ico" />
	</head>

<style>
	:root { --mainColor:#000000; --whiteColor:#3b5568; }
	html, body { font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif; font-size:16px; background:#0a2b43; padding:0; margin:0; color:#799fbb; }
	#main_header { border-bottom:#fff6d9 1px solid; height:44px; width:100%; margin:0 auto; background:#0c1d2a; position:fixed; top:0; z-index:100; }
	#header { width:95%; margin:0 auto; height:44px; position:relative; }
	#main_breadcrumbs { width:95%; margin:0 auto; padding-bottom:10px; padding-top:50px; }
	#main_body { width:95%; margin:0 auto; }
	.GalleryItem { display:inline-block; width:calc(100%/6); height:300px; background-size:cover; position:relative; background-position-x:center; }
	.GalleryItem:hover{ animation: bounce 0.4s; }
	.GalleryInfo { position:absolute; bottom:0; width:100%; color:#fff6d9; font-size:0.8em; line-height:0.9em; padding:0.4em; opacity:0.7; box-sizing:border-box; background:linear-gradient(to right, var(--mainColor) 0%, var(--mainColor) 5px, transparent); background-repeat:repeat-x; background-size:100%; font-weight:normal; }
	.GalleryOne { display:block; padding-bottom:1em; width:100%; text-align:center; }
	img { max-width:100%; max-height:100%; }
	a { font-weight:bold; color:#fff6d9; text-decoration:none; }
	#htitle { font-size:2.0em; font-family:Courier New; float:left; width:80%; display:inline-block; }
	#htitle img { position:relative; top:2px; padding-right:3px; vertical-align:sub; }
	#contact { float:right; width:15%; display:inline-block; height:30px; padding-top:6px; text-align:right; cursor:pointer; }
	#main_breadcrumbs { font-size:0.8em; }
	@keyframes bounce {
		0%, 20%, 60%, 100% { -webkit-transform: translateY(0); transform: translateY(0); }
		40% { -webkit-transform: translateY(-20px); transform: translateY(-20px); }
		80% { -webkit-transform: translateY(-10px); transform: translateY(-10px); }
	}
	#ContactDiv { z-index:1000; height:40%; position:fixed; top:10%; left:10%; width:80%; background:#fff6d9; color:#0c1d2a; opacity:0.9; font-size:2em; text-align:center; padding-top:7%; display:none; border-radius:50px; box-shadow:10px 10px 5px 0px rgba(0,0,0,0.75); }
	.x { position:absolute; top:0; right:30; font-size:0.5em; cursor:pointer; }

	#One { width:100%; height:88%; position:relative; }
	#OneDol { position:absolute; bottom:0; height:18%; width:100%; display:flex; }
	#OneGora { position:absolute; top:0; height:80%; width:100%; text-align:center; }
	#OneDol img { height:auto; }
	.ThumbItem { display:inline-block; background-size:cover; position:relative; background-position-x:center; margin-right:4px; }
	.ThumbItem:hover{ animation: bounce 0.4s; }
	#PicPrev { position:absolute; left:0; width:15%; height:80%; opacity:0.7; z-index:10; cursor:pointer; }
	#PicNext { position:absolute; right:0; width:15%; height:80%; opacity:0.7; z-index:10; cursor:pointer; }

	.prev-next-button { position:absolute; top:50%; width:60px; height:60px; border-radius:50%; background:white; transform:translateY(-50%); }
	.prev-next-button svg { position:absolute; left:20%; top:20%; width:60%; height:60%; }
	.previous { left:10px; }
	.next { right:10px; }

	@media all and (max-width: 900px) {
		.GalleryItem { width:calc(100%/3); }
		#main_header { height:28px; }
		#header { height:28px; }
		#htitle { font-size:1.2em; }
		#htitle img { height:25px; position:relative; top:2px; }
		#main_breadcrumbs { padding-top:35px; }
		#contact { height:25px; padding-top:1px; }
		#ContactDiv { font-size:1em; line-height:1.5em; }
		.prev-next-button { width:30px; height:30px; }
	}
	@media all and (max-width: 500px) {
		.GalleryItem { width:calc(100%/2); height:250px; }
	}
</style>

<script>
	function ShowPic(t) {
		document.getElementById('Gimg').src=pics[(t-1)%pics.length];
		showCount();
	}
	function NextPic() {
		var akt=document.getElementById('Gimg').src;
		var aktIndex = pics.indexOf(akt);
		document.getElementById('Gimg').src=pics[(aktIndex+1)%pics.length];
		showCount();
	}
	function PrevPic() {
		var akt=document.getElementById('Gimg').src;
		var aktIndex = pics.indexOf(akt);
		var show=aktIndex-1;
		if(show=='-1') show=pics.length-1;
		document.getElementById('Gimg').src=pics[show];
		showCount();
	}
	document.onkeydown = checkKey;
	function checkKey(e) {
		e = e || window.event;
		if (e.keyCode == '37') { PrevPic(); } // left arrow
		else if (e.keyCode == '39') { NextPic(); } // right arrow
	}
	function showCount() {
		var akt=document.getElementById('Gimg').src;
		var aktIndex = pics.indexOf(akt);
		var allIndex = pics.length;
		document.getElementById('licznik').innerHTML=(aktIndex+1)+' / '+allIndex;
		window.location.hash=(aktIndex+1);
	}
</script>	

<body>

<div id="ContactDiv">
	<div class="x" onclick="document.getElementById('ContactDiv').style.display='none';">zamknij x</div>
	Zapraszam agencje, firmy, kluby sportowe<br/>i osoby prywatne do kontaktu<br/>
</div>

<div id="main_header">
	<div id="header">
		<div id="htitle"><a href="//<?php echo $base_href; ?>"><img src="logo.png"> Galeria</a></div>
		<div id="contact" onclick="document.getElementById('ContactDiv').style.display='block';"><img src="contactme.png"></div>
	</div>
</div>

<div id="main_breadcrumbs">
	<a href="//<?php echo $base_href; ?>/">Home</a> &bull; <?php if(isset($_REQUEST['g'])) { echo str_replace('_',' ',$_REQUEST['g']); } ?>
</div>

<div id="main_body">
<?php
if(isset($_REQUEST['g'])) { //pojedyncza galeria
	$IleFot=0;
	$pics=''; $dol='';
	foreach(glob('photos/'.str_replace('_',' ',$_REQUEST['g']).'/*') as $plik) {
		if(preg_match('/.jpg/i',$plik)) {
			$IleFot++;
			$pics.='"https://4891.me/'.str_replace(' ','%20',$plik).'", ';
			$dol.='<div class="ThumbItem" style="width:calc(100%/%IleFot%); background-image:url(\''.$plik.'\'" onclick="document.getElementById(\'Gimg\').src=\''.$plik.'\'; showCount();" onmouseover="document.getElementById(\'Gimg\').src=\''.$plik.'\'; showCount();"></div>';
			if($IleFot==1) $gora='<img id="Gimg" src="'.$plik.'" alt="" />';
		}
	}

  echo '<script> var pics = ['.substr($pics,0,-2).']; </script>';
	echo '<div id="One">';
	echo ' <div id="licznik" style="position:absolute; top:0; right:0; z-index:1000; padding:0.3em; color:#fff6d9;">1 / '.$IleFot.'</div>'; 	
	echo ' <div id="PicPrev" onclick="PrevPic()"><div class="prev-next-button previous"><svg viewbox="0 0 100 100"><path class="arrow" d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" transform=" translate(15,0)"></svg></div></div>';
	echo ' <div id="OneGora">'.$gora.'</div>';
	echo ' <div id="PicNext" onclick="NextPic()"><div class="prev-next-button next"><svg viewbox="0 0 100 100"><path class="arrow" d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z "transform="translate(85,100) rotate(180) "></svg></div></div>';
	echo ' <div id="OneDol">'.str_replace('%IleFot%',$IleFot,$dol).'</div>';
	echo '</div>';
	//print_r($_GET);
	?>
	<script>
	hash=window.location.hash; 
	if(hash!='') {
		hash=hash.replace("#", "");
		if(hash<=pics.length) ShowPic(hash);
	}
	</script>
	<?php
}
else { //wyswietl liste
	//stworz katalogi
	$dirs='photos/*';
	foreach(glob($dirs, GLOB_ONLYDIR) as $dir) {
		$katalogi[]=$dir;
	}
	rsort($katalogi);
	//pr($katalogi);
	//sprawdz pliki
	foreach($katalogi as $k=>$v) {
		foreach(glob($v.'/*') as $plik) {
			if(preg_match('/i.jpg/i',$plik)) {
				$v=str_replace('photos/','',$v);
				echo '<a href="galeria/'.str_replace(' ','_',$v).'">';
				echo '<div class="GalleryItem" style="background-image:url(\''.$plik.'\');">';
				echo ' <div class="GalleryInfo"><span style="font-size:0.8em;">'.substr($v,0,10).'</span><br/>'.substr($v,10,100).'</div>';
				echo '</div>';
				echo '</a>';
			}
		}
	}
}
?>
</div>
</body>
</html>
