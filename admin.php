<?php
require('config.php');
if(($_POST['aa']=='raz') AND ($_REQUEST["haslo"]==$haslo)) {
	$_SESSION["haslo"]=$haslo;
}

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n";
echo '<html xmlns="http://www.w3.org/1999/xhtml">'."\n";
echo '<head>'."\n";
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'."\n";
echo '<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">';
echo '<meta name="robots" content="noindex nofollow" />';
echo '<title>admin</title>'."\n";
echo '</head>'."\n";
echo '<body style="font-family:\'Open Sans\'; margin:0; padding:0;">'."\n";

if(isset($_REQUEST['cat'])) echo '<div style="width:75%; height:50px; padding:10px; float:right; line-height:50px; font-size:1.5em; text-overflow:ellipsis; white-space: nowrap; color:#b90000;">'.$_REQUEST['cat'].'</div>'."\n";
//echo '<br style="clear:both;"/>';

echo '<style>'."\n";
echo '.button { padding:5px; border:1px solid #999; background:#f1f1f1; font-size:1em; font-family:\'Open Sans\'; border-radius:5px; -moz-border-radius:5px; -web-kit-border-radius:5px; -khtml-border-radius:5px; }'."\n";
echo 'a { text-decoration:none; color:#b90000; }'."\n";
echo '</style>'."\n";

if($_SESSION["haslo"]!=$haslo) {
	echo '<form action="admin.php" method="post">'."\n";
	echo ' <input type="password" name="haslo" class="button" style="width:300px;">'."\n";
	echo ' <input type="hidden" name="aa" value="raz" />'."\n";
	echo ' <input type="submit" value="dalej" class="button" style="background:lightgreen; width:80px;">'."\n";
	echo '</form>'."\n";
}
else {
	if(!isset($_REQUEST['cat'])) $_REQUEST['cat']='';
	//dodaj katalog
	if($_REQUEST['addcat']=='1') {
		mkdir("photos/".$_REQUEST['nazwa'], 0775);
		header('Location: https://'.$base_href.'/admin.php?cat='.$_REQUEST['nazwa'].'');
	}
	//zmien thumb katalogu
	if($_REQUEST['o']) {
		unset($arr);
		if ($handle = opendir('photos/'.$_REQUEST['cat'])) {
			while (false !== ($entry = readdir($handle))) { if ($entry != "." && $entry != ".." && $entry!='index.php' && $entry!='th') { $arr[]=$entry; } }
			closedir($handle);
		}
		if($arr) {
			sort($arr);
			foreach ($arr as $k=>$v) {
				if(preg_match('/i\./',$v)) {
					$cat=$_REQUEST['cat'];
					$oldf=$v;
					$newf=str_replace('i.','.',$v);
					rename('photos/'.$cat.'/'.$oldf.'', 'photos/'.$cat.'/'.$newf.'');
					rename('photos/'.$cat.'/th/'.$oldf.'', 'photos/'.$cat.'/th/'.$newf.'');
				}
			}
		}
		$cat=$_REQUEST['cat'];
		$oldf=$_REQUEST['o'];
		$newf=str_replace('.','i.',$oldf);
		rename('photos/'.$cat.'/'.$oldf.'', 'photos/'.$cat.'/'.$newf.'');
		rename('photos/'.$cat.'/th/'.$oldf.'', 'photos/'.$cat.'/th/'.$newf.'');
	}
	//dodaj pliki
	if((isset($_REQUEST['cat'])) AND ($_REQUEST['raz']=='1')) {
		foreach($_FILES['pliki']['error'] as $k=>$v) {
			if($_FILES['pliki']['error'][$k]=='0') {
				$tmp_name = $_FILES["pliki"]["tmp_name"][$k];
				$nazwa=strtolower($_FILES["pliki"]["name"][$k]);
				$nazwa=str_replace(' ','-',$nazwa);
				$nazwa=str_replace('.jpg','',$nazwa);
				$nazwa=str_replace('.png','',$nazwa);
				$nazwa.='-'.time();
				move_uploaded_file($tmp_name, 'photos/'.$_REQUEST['cat'].'/'.$nazwa.'.jpg');
				$komunikat.='Plik dodany - '.$_FILES["pliki"]["name"][$k].'<br/>';
				sleep(2);
			}
			elseif($_FILES['pliki'][$k]=='1') {
				$komunikat.='Błąd - wielkość pliku przekracza wartość maksymalną - '.$_FILES["pliki"]["name"][$k].'<br/>';
			}
			elseif($_FILES['pliki'][$k]=='2') {
				$komunikat.='Błąd - wielkość pliku przekracza wartość maksymalną - '.$_FILES["pliki"]["name"][$k].'<br/>';
			}
			elseif($_FILES['pliki'][$k]=='3') {
				$komunikat.='Błąd - plik został wysłany częściowo - '.$_FILES["pliki"]["name"][$k].'<br/>';
			}
			elseif($_FILES['pliki'][$k]=='4') {
				$komunikat.='Błąd - żaden plik nie został wysłany - '.$_FILES["pliki"]["name"][$k].'<br/>';
			}
			elseif($_FILES['pliki'][$k]=='6') {
				$komunikat.='Błąd - problem z folderem temp - '.$_FILES["pliki"]["name"][$k].'<br/>';
			}
			elseif($_FILES['pliki'][$k]=='7') {
				$komunikat.='Błąd - błąd zapisu na dysk - '.$_FILES["pliki"]["name"][$k].'<br/>';
			}
			elseif($_FILES['pliki'][$k]=='8') {
				$komunikat.='Błąd - błąd php - '.$_FILES["pliki"]["name"][$k].'<br/>';
			}
		}
	}
	
	//listuj katalogi po lewej - poczatek
	{
		unset($arr);
		if ($handle = opendir('photos/')) {
			while (false !== ($entry = readdir($handle))) { if ($entry != "." && $entry != ".." && $entry!='index.php') { $arr[]=$entry; } }
			closedir($handle);
		}
		echo '<div id="scrollDiv" style="float:left; width:300px; padding:0 10px; margin-right:5px; background:#ededed; overflow:auto;">'."\n";
		if($arr) {
			rsort($arr);
			foreach ($arr as $k=>$v) {
				echo '<a href="admin.php?cat='.$v.'" text-overflow:ellipsis; white-space:nowrap;">'.$v.'</a><br/>';
			}
		}
		else {
			echo 'Brak galerii'; 
		}
		echo '</div>'."\n";
	}
	//listuj katalogi po lewej - koniec

	if($_REQUEST['cat']) {
		echo '<div id="prawoDiv" style="float:right; overflow:auto;">'."\n";
		if($komunikat) { echo '<div class="row">'.$komunikat.'</div>'; }
		//pokaz istniejace zdjecia poczatek
		{
			unset($arr);
			if ($handle = opendir('photos/'.$_REQUEST['cat'])) {
				while (false !== ($entry = readdir($handle))) { if ($entry != "." && $entry != ".." && $entry!='index.php' && $entry!='th') { $arr[]=$entry; } }
				closedir($handle);
			}
			echo '<div>'."\n";
			if($arr) {
				sort($arr);
				foreach ($arr as $k=>$v) {
					$border='';
					if(preg_match('/i.jpg/i',$v)) { $border=" border:3px solid red;"; }
					if(!file_exists('photos/'.$_REQUEST['cat'].'/th/'.$v.'')) {
						echo '<a href="admin.php?cat='.$_REQUEST['cat'].'&o='.$v.'"><img src="createthumb.php?filename=photos/'.$_REQUEST['cat'].'/'.$v.'&amp;size=150" style="padding:1px;'.$border.'" /></a>';
					}
					else {
						echo '<a href="admin.php?cat='.$_REQUEST['cat'].'&o='.$v.'"><img src="photos/'.$_REQUEST['cat'].'/th/'.$v.'" style="padding:1px;'.$border.'"></a>';
					}
				}
			}
			else {
				echo 'Brak zdjęć w galerii';
			}
			echo '</div>'."\n";
		}
		//pokaz istniejace zdjecia koniec

		//dodaj nowe zdjecia poczatek
		{
			echo '<div style="clear:both; font-size:1.5em; font-weight:bold; padding-top:0.5em;">dodaj nowe zdjęcia<br/>';
			echo '<form action="?cat='.$_REQUEST['cat'].'" method="post" enctype="multipart/form-data" class="col s12">';
			echo ' <input type="hidden" name="raz" value="1">';
			echo ' <input type="hidden" name="c" value="'.$_REQUEST['cat'].'">';
			echo ' <input type="file" name="pliki[]" multiple>';
			echo ' <button type="submit" name="action">Prześlij pliki</button>';
			echo '</form>';
			echo '</div>';
		}
		//dodaj nowe zdjecia koniec
	}
	else {
		//dodaj katalog
		{
			echo '<div style="">'."\n";
			echo '<form action="admin.php" method="post">'."\n";
			echo ' <input type="hidden" name="addcat" value="1">';
			echo ' <input type="hidden" name="subcat" value="'.$_REQUEST['cat'].'">';
			echo ' <b>dodaj nową galerię</b><br/>'."\n";
			echo ' <input type="text" name="nazwa" size="90" class="button" style="width:500px;">'."\n";
			echo ' <input class="button" type="submit" value="dodaj" style="background:lightgreen; width:80px;"><br/>'."\n";
			echo ' nazwa katalogu bez polskich znaków (format yyyy-mm-dd nazwa)'."\n";
			echo'</form>'."\n";
			echo '</div>'."\n";
		}
		//dodaj katalog
		echo '<br/><br/>';
		//help
		echo '&bull; czerwona obwódka w galerii oznacza zjęcie nagłówkowe<br/>';
		
	}
	echo '<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>'."\n";
	echo '<script>'."\n";
	echo 'a=$(window).height();'."\n";
	echo 'a=a-100;'."\n";
	echo 'b=$(window).width();'."\n";
	echo 'b=b-330;'."\n";
	echo '$("#scrollDiv").css("max-height",a);'."\n";
	echo '$("#prawoDiv").css("max-height",a);'."\n";
	echo '$("#prawoDiv").css("width",b);'."\n";
	echo '</script>'."\n";
}
echo '</body>'."\n";
echo '</html>'."\n";
?>
